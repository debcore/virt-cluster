# virt-cluster

The shortest distance to a working scalable production-quality libvirt-based single-or-multi-host virtualization cluster.

## How It Works

Let's keep things really simple. This creates a REST API for libvirt and simplifies certain tasks. It stores metadata in libvirt and therefore doesn't require a key/value store or database. There is no queue or other pull model for scheduling.

This setup requires at least one dedicated phyiscal disk in a computer running linux. 

To save some disk space, I added an optional call to virt-resize. This currently only works for amd64 base images; virt-resize renumbers partitions and screws with my EFI implementation on arm64. 

There is no auth. Auth would be implemented at the proxy (nginx) layer. TODO

## Installation

1. First things first, head over to https://gitlab.com/debcore/debcore-images and generate yourself an amd64 and/or arm64 base image. You could use your own base images, but this is the easiest way to get started. 
2. If you plan on using one disk (eg. /dev/sdb), you're ready to go. If you want to use a RAID, you should create your RAID device, then use that device as your disk (eg. /dev/md0)
3. Install all the packages you need. `apt-get -y install libvirt-daemon-system`
4. Set up your LVM group. I use 'libvirt-pool' as my default pool for base images. You can call it anything so long as you update the templates to reflect the name.
```
MYDISK=/dev/md0

# define the pool
virsh pool-define-as libvirt-pool logical - - $MYDISK libvirt_pool /dev/libvirt_pool
# create the lvm devices
virsh pool-build libvirt-pool
# load the lvm devices
virsh pool-start libvirt-pool
# set pool to autostart
virsh pool-autostart libvirt-pool
```
5. Import the base image you created in step 1:
```
virsh vol-create-as libvirt-pool debcore_guest_latest_amd64 2G
dd if=MYIMG.img of=/dev/libvirt_pool/debcore_guest_latest_amd64 bs=1M status=progress
```
