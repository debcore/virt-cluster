from flask import Flask, request, send_from_directory, render_template, redirect, url_for, Response

from functools import wraps

import libvirt
import re

from vm import *

app = Flask(__name__)

def check_auth(username, password):
  """This function is called to check if a username /
  password combination is valid.
  """
  return username == 'admin' and password == 'admin'

def authenticate():
  """Sends a 401 response that enables basic auth"""
  return Response(
  'Could not verify your access level for that URL.\n'
  'You have to login with proper credentials', 401,
  {'WWW-Authenticate': 'Basic realm="Login Required"'})

def parse_input(form, config={}):
  # arch: arm64
  # publicIP: 65.102.45.156/29
  # privateIP: 192.168.0.102/24
  # publicGW: 65.102.45.158
  # privateGW: 192.168.0.1
  # memoryKB: 8388608
  # cpuCount: 8

  if 'arch' not in config:
    config['arch'] = form.get('arch','amd64')
  if 'publicIP' not in config:
    config['publicIP'] = form.get('publicIP','')
  if 'privateIP' not in config:
    config['privateIP'] = form.get('privateIP','')
  if 'publicGW' not in config:
    config['publicGW'] = form.get('publicGW','')
  if 'privateGW' not in config:
    config['privateGW'] = form.get('privateGW','')
  if 'memoryKB' not in config:
    config['memoryKB'] = form.get('memoryKB', 2097152)
  if 'cpuCount' not in config:
    config['cpuCount'] = form.get('cpuCount',1)
  if 'init_script' not in config:
    config['init_script'] = form.get('init_script','init.sh.tmpl')
  if 'dmi_chunk_size' not in config:
    config['dmi_chunk_size'] = form.get('dmi_chunk_size', 1023)
  if 'lvm_pool' not in config:
    config['lvm_pool'] = form.get('lvm_pool', 'libvirt-pool')
  if 'sizeGB' not in config:
    config['sizeGB'] = form.get('sizeGB', 2)
  if 'resizeGB' not in config:
    config['resizeGB'] = form.get('resizeGB',0)

  # set up the partition numbers correctly for x86 vs arm
  config['efipart'] = 1
  config['rootpart'] = 2
  if config['arch'] == 'arm64':
    config['efipart'] = 15
    config['rootpart'] = 1

  config['libvirt_template'] = 'libvirt_guest_%s.xml.tmpl' % config['arch']
  config['base_volume'] = 'debcore_guest_latest_%s' % config['arch']

  return config

def get_current_values(vm):
  conn = libvirt.open()
  dom = conn.lookupByName(vm)
  info = dom.info()
  state,max_memKB,memKB,cpuCount,cpuTime = dom.info()
  current = {}
  current['memoryKB'] = memKB
  current['cpuCount'] = cpuCount
  metadata = dom.metadata(2,'http://localhost/xml/0',libvirt.VIR_DOMAIN_AFFECT_CONFIG)
  res = re.search('<privateIP>(.*)</privateIP>',metadata)
  if res is not None:
    current['privateIP'] = res.group(1)
  res = re.search('<publicIP>(.*)</publicIP>',metadata)
  if res is not None:
    current['publicIP'] = res.group(1)
  res = re.search('<privateGW>(.*)</privateGW>',metadata)
  if res is not None:
    current['privateGW'] = res.group(1)
  res = re.search('<publicGW>(.*)</publicGW>',metadata)
  if res is not None:
    current['publicGW'] = res.group(1)
  return current

def requires_auth(f):
  @wraps(f)
  def decorated(*args, **kwargs):
    auth = request.authorization
    if not auth or not check_auth(auth.username, auth.password):
      return authenticate()
    return f(*args, **kwargs)
  return decorated

@app.route('/')
def slash():
  return redirect(url_for('apiroot'))

@app.route('/api')
def apiroot():
  return 'Usage: /api/<vm>\n', 200

@app.route('/api/<vm>', methods=['GET'])
def status(vm):
  return 'GET\n', 200

@app.route('/api/<vm>', methods=['POST'])
def create(vm):
  config = parse_input(request.form)
  dom = create_guest(vm,**config)
  return 'CREATE\n', 200

@app.route('/api/<vm>', methods=['DELETE'])
def delete(vm):
  delete_guest(vm)
  return 'DELETE\n', 200

@app.route('/api/<vm>', methods=['PATCH'])
def update(vm):
  current = get_current_values(vm)
  config = parse_input(request.form,current)
  return 'UPDATE\n', 200

if __name__ == "__main__":
  app.run(debug=True,host='0.0.0.0')

