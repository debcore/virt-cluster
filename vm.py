
from jinja2 import Environment
import sys
import yaml
import base64
import libvirt
import subprocess

conn = libvirt.open()

def create_chunks_from_text(vm_name,**kwargs):
  # PUBLIC_IP={{ publicIP }}
  # PRIVATE_IP={{ privateIP }}
  # HOST={{ host }}
  # PUBLIC_GW={{ publicGW }}
  # PRIVATE_GW={{ privateGW }}
  tmpl = open(kwargs['init_script']).read()
  txt = Environment().from_string(tmpl).render(vm_name=vm_name,**kwargs)
  encoded_txt = base64.b64encode(txt)
  chunks = [encoded_txt[i:i+kwargs['dmi_chunk_size']] for i in range(0, len(encoded_txt), kwargs['dmi_chunk_size'])]
  if len(chunks) > 1:
    # since the guest only reads the first oemtable if we have more 
    # we need to build an index in the first table to read the others
    first_table_txt = """#!/bin/bash\nfor t in {2..%s}; do dmidecode --oem-string $t; done | tr -d '\\n' | base64 -d -w0 | bash""" % str(len(chunks) + 1)
    chunks = [base64.b64encode(first_table_txt)] + chunks
  return chunks

def create_xml_from_template(vm_name,chunks,**kwargs):
  guest_tmpl = open(kwargs['libvirt_template']).read()
  guest_txt = Environment().from_string(guest_tmpl).render(vm_name=vm_name,chunks=chunks,values=kwargs)
  return guest_txt

def create_volume(vm_name, **kwargs):
  dev_name = "/dev/%s" % kwargs['lvm_pool'].replace('-','_')
  pool = conn.storagePoolLookupByName(kwargs['lvm_pool'])
  base_volume = pool.storageVolLookupByName(kwargs['base_volume'])
  size_gb = kwargs['sizeGB']
  if kwargs['resizeGB'] > 0:
    size_gb = kwargs['resizeGB']
  newvol_xml = """<volume type='block'>\n  <name>%s</name>\n  <capacity unit="G">%s</capacity>\n  <target>\n    <path>%s/%s</path>\n  </target>\n</volume>\n""" % (vm_name,str(size_gb),dev_name,vm_name)
  newvol = pool.createXMLFrom(newvol_xml, base_volume, 0)
  if kwargs['resizeGB'] > 0 and kwargs['arch'] == 'amd64':
    subprocess.call(['/usr/bin/virt-resize', '--expand', '/dev/vda%s' % kwargs['rootpart'], '%s/%s' % (dev_name,kwargs['base_volume']), '%s/%s' % (dev_name,vm_name) ])
  return newvol

def create_guest(vm_name, **kwargs):
  dev_name = "/dev/%s" % kwargs['lvm_pool'].replace('-','_')
  chunks = create_chunks_from_text(vm_name,**kwargs)
  guest_txt = create_xml_from_template(vm_name,chunks,**kwargs)
  newvol = create_volume(vm_name,**kwargs)

  dom = conn.defineXML(guest_txt)
  dom.create()
  dom.setAutostart(1)

  return dom

def delete_guest(vm_name):
  dom = conn.lookupByName(vm_name)
  dom.destroy()
  dom.undefine()
  vol = conn.storageVolLookupByPath('/dev/libvirt_pool/%s' % vm_name)
  vol.delete()
  return True
